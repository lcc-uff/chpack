# Compute Effective PERMEABILITY Properties
using JSON
using SparseArrays
using LinearAlgebra

# Model data struct:
struct Model
    nx::UInt64;
    ny::UInt64;
    nz::UInt64;
    voxelSize::Float64;
    refinement::UInt64;
    rhsType::UInt8;
    solverType::UInt8;
    pcgTol::Float64;
    pcgIter::UInt64;
    nNodes::UInt64;
    nElems::UInt64;
    nDOFs::UInt64;
    nVelocityNodes::UInt64;
    nInterfaceNodes::UInt64;
    DOFMap::Vector{UInt64};
    FluidElems::Vector{UInt64};
    function Model(_nx, _ny, _nz, _voxelSize, _refinement, _rhsType, _solverType, _pcgTol, _pcgIter, _nNodes, _nElems, _nDOFs, _nVelocityNodes, _nInterfaceNodes, _DOFMap, _FluidElems)
        new(_nx, _ny, _nz, _voxelSize, _refinement, _rhsType, _solverType, _pcgTol, _pcgIter, _nNodes, _nElems, _nDOFs, _nVelocityNodes, _nInterfaceNodes, _DOFMap, _FluidElems);
    end
end

# Build Model:
function buildModel(_JsonFile::String, _RawFile::String)
    println(".Building Model!")
    # Read Json file:
    nx, ny, nz, voxelSize, refinement, rhsType, solverType, pcgTol, pcgIter, matKeys = readJSON(_JsonFile)
    # Read Raw file:
    elemMatMap = zeros(UInt8, nx * ny * nz * refinement * refinement * refinement)
    readRAW!(nx, ny, nz, refinement, elemMatMap, _RawFile)
    # Update the parameters based on the given refinement level:
    nx *= refinement
    ny *= refinement
    nz *= refinement
    nNodes::UInt64 = (nx + 1) * (ny + 1) * (nz + 1)
    nElems::UInt64 = (nx) * (ny) * (nz)
    DOFperNode::UInt64 = 4
    nDOFs::UInt64 = nElems * DOFperNode
    # Generate a map of Degree of Freedom:
    DOFMap = zeros(UInt64, nNodes)
    nVelocityNodes, nInterfaceNodes, nDOFs, FluidElems = generateDOFMap!(nx, ny, nz, elemMatMap, DOFMap) 
    # Build the Model:
    model = Model(nx, ny, nz, voxelSize, refinement, rhsType, solverType, pcgTol, pcgIter, nNodes, nElems, nDOFs, nVelocityNodes, nInterfaceNodes, DOFMap, FluidElems)
    println("---------------------------")
    return model
end

# Read JSON file:
function readJSON(_filename::String)
    println("   .Reading JSON!")
    # Open and read file:
    open(_filename, "r") do f
        data = JSON.parse(f)
        nx::UInt64 = data["image_dimensions"][1]
        ny::UInt64 = data["image_dimensions"][2]
        nz::UInt64 = data["image_dimensions"][3]
        refinement::UInt64 = 1
        if haskey(data, "refinement"); refinement = data["refinement"]; end
        voxelSize::Float64 = 1.0
        if haskey(data, "voxel_size"); voxelSize = data["voxel_size"]; end
        rhsType::UInt8 = 0
        if haskey(data, "type_of_rhs"); rhsType = data["type_of_rhs"]; end
        solverType::UInt8 = 0
        if haskey(data, "type_of_solver"); solverType = data["type_of_solver"]; end
        pcgTol::Float64 = 0.000001
        if haskey(data, "solver_tolerance"); pcgTol = data["solver_tolerance"]; end
        pcgIter::UInt64 = nx * ny * refinement * refinement
        if haskey(data, "number_of_iterations"); pcgIter = data["number_of_iterations"]; end
        nMat::UInt16 = data["number_of_materials"]
        materials = data["properties_of_materials"]
        matKeys = zeros(UInt16, 256)
        for i = 1:nMat
            matKeys[convert(UInt8, materials[i][1]) + 1] = i
        end
        materials = nothing
        data = nothing
        return nx, ny, nz, voxelSize, refinement, rhsType, solverType, pcgTol, pcgIter, matKeys
    end
end

# Read RAW file:
function readRAW!(_nx::UInt64, _ny::UInt64, _nz::UInt64, _refinement::UInt64, _elemMatMap::Vector{UInt8}, _filename::String)
    println("   .Reading RAW!")
    # Initializations
    rows = _ny; cols = _nx; slices = _nz;
    nx = _nx * _refinement
    ny = _ny * _refinement
    # Open and read file
    io = open(_filename, "r")
    for k = 1:slices
        for i = 1:rows
            for j = 1:cols
                if (!eof(io))
                    buffer = read(io, UInt8)
                    if (buffer > 1)
                        buffer = 1
                    end
                    for kk = (_refinement * (k - 1) + 1):(_refinement * k)
                        for ii = (_refinement * (i - 1) + 1):(_refinement * i)
                            for jj = (_refinement * (j - 1) + 1):(_refinement * j)
                                _elemMatMap[ii + (jj - 1) * ny + (kk - 1) * nx * ny] = buffer
                            end
                        end
                    end
                else # if reached end of file before expected, return.
                    close(io)
                end
            end
        end
    end
    close(io)
    buffer = nothing
end

# Generate the Degree of Freedom Map:
function generateDOFMap!(_nx::UInt64, _ny::UInt64, _nz::UInt64, _elemMatMap::Vector{UInt8}, _DOFMap::Vector{UInt64})
    println("   .Generating the Map of DOFs (Degrees of Freedom)!")
    # Initializations
    nNodes::UInt64 = (_nx + 1) * (_ny + 1) * (_nz + 1)
    NodesperSlice::UInt64 = (_nx + 1) * (_ny + 1)
    ElemsperSlice::UInt64 = _nx * _ny
    numVelocityNodes::UInt64 = 0
    numInterfaceNodes::UInt64 = 0
    numFluidElems::UInt64 = 0
    slice::UInt64 = 0; c::UInt64 = 0; r::UInt64 = 0;
    vFluidElems = zeros(UInt64, nNodes)
    @fastmath @inbounds @simd for N = 1:nNodes
        # Nodes's column and row
        slice = (N - 1) ÷ ((_nx + 1) * (_ny + 1)) + 1
        c = ((N - (slice - 1) * NodesperSlice - 1) ÷ (_ny + 1)) + 1
        r = (N - (slice - 1) * NodesperSlice - (c - 1) * (_ny + 1))
        # Flags - Boundary nodes
        flag_lastcol = (c / (_nx + 1) == 1)
        flag_lastrow = (r / (_ny + 1) == 1)
        flag_lastslice = (slice / (_nz + 1) == 1)
        flag_firstcol = (c == 1)
        flag_firstrow = (r == 1)
        flag_firstslice = (slice == 1)
        # Check Neighboring   |nw|ne|
        #                      - N - 
        #                     |sw|se|
        elem_se_back = N - (slice - 1) * NodesperSlice - c + 1 + (slice - 1) * ElemsperSlice + flag_lastrow * (-_ny) + flag_lastcol * (-_nx * (_ny + 1) + c - 1) + flag_lastslice * (-(slice - 1) * ElemsperSlice)
        elem_ne_back = N - (slice - 1) * NodesperSlice - c + (slice - 1) * ElemsperSlice + flag_firstrow * (_ny) + flag_lastcol * (-_nx * _ny) + flag_lastslice * (-(slice - 1) * ElemsperSlice)  
        elem_nw_back = N - (slice - 1) * NodesperSlice - c + (slice - 1) * ElemsperSlice - _ny + flag_firstrow * (_ny) + flag_firstcol * (_nx) * _ny + flag_lastslice * (-(slice - 1) * ElemsperSlice)                          
        elem_sw_back = N - (slice - 1) * NodesperSlice - c + 1 + (slice - 1) * ElemsperSlice - _ny + flag_firstcol * (_nx) * _ny + flag_lastrow * (-_ny) + flag_lastslice * (-(slice - 1) * ElemsperSlice)
        elem_se_front = elem_se_back - ElemsperSlice + flag_firstslice * (_nz * ElemsperSlice) + flag_lastslice * (_nz * ElemsperSlice)
        elem_ne_front = elem_ne_back - ElemsperSlice + flag_firstslice * (_nz * ElemsperSlice) + flag_lastslice * (_nz * ElemsperSlice)
        elem_nw_front = elem_nw_back - ElemsperSlice + flag_firstslice * (_nz * ElemsperSlice) + flag_lastslice * (_nz * ElemsperSlice)
        elem_sw_front = elem_sw_back - ElemsperSlice + flag_firstslice * (_nz * ElemsperSlice) + flag_lastslice * (_nz * ElemsperSlice)
        # # Flags - type of node (At least one fluid elem / At least one solid elem / Only fluid elems)
        flag_oneFluidElem =  - (_elemMatMap[elem_se_front] * _elemMatMap[elem_ne_front] * _elemMatMap[elem_nw_front] * _elemMatMap[elem_sw_front] * _elemMatMap[elem_se_back] * _elemMatMap[elem_ne_back] * _elemMatMap[elem_nw_back] * _elemMatMap[elem_sw_back] - 1)
        flag_oneSolidElem =  (_elemMatMap[elem_se_front] + _elemMatMap[elem_ne_front] + _elemMatMap[elem_nw_front] + _elemMatMap[elem_sw_front] + _elemMatMap[elem_se_back] + _elemMatMap[elem_ne_back] + _elemMatMap[elem_nw_back] + _elemMatMap[elem_sw_back]) > 0
        flag_onlyFluidElems =  (_elemMatMap[elem_se_front] + _elemMatMap[elem_ne_front] + _elemMatMap[elem_nw_front] + _elemMatMap[elem_sw_front] + _elemMatMap[elem_se_back] + _elemMatMap[elem_ne_back] + _elemMatMap[elem_nw_back] + _elemMatMap[elem_sw_back]) == 0
        # Creation of DofMap
        numVelocityNodes += flag_onlyFluidElems * (1 - flag_lastrow) * (1 - flag_lastcol) * (1 - flag_lastslice)
        numInterfaceNodes += flag_oneSolidElem * flag_oneFluidElem * (1 - flag_lastrow) * (1 - flag_lastcol) * (1 - flag_lastslice)  
        value_CaseVelocity = numVelocityNodes
        value_CaseInterface = nNodes + numInterfaceNodes        
        _DOFMap[N] = flag_onlyFluidElems * value_CaseVelocity + flag_oneFluidElem * flag_oneSolidElem * value_CaseInterface
        # Application of PBC 
        top_node = flag_lastrow * (N - _ny) + (1 - flag_lastrow)
        left_node = flag_lastcol * (N - _nx * (_ny + 1)) + (1 - flag_lastcol)  
        front_node = flag_lastslice * (N - (slice - 1) * NodesperSlice) + (1 - flag_lastslice)
        _DOFMap[N] += (1 - flag_lastslice) * (flag_lastrow * (-_DOFMap[N] + _DOFMap[top_node]) + flag_lastcol * (-_DOFMap[N] + _DOFMap[left_node]) + flag_lastrow * flag_lastcol * (+_DOFMap[N] - _DOFMap[left_node])) + flag_lastslice * (-_DOFMap[N] + _DOFMap[front_node]) 
        # Vector of Fluid Elements
        numFluidElems += (_elemMatMap[elem_se_back] == 0) * (1 - flag_lastrow) * (1 - flag_lastcol) * (1 - flag_lastslice);
        vFluidElems[numFluidElems * (_elemMatMap[elem_se_back] == 0) + (_elemMatMap[elem_se_back] != 0)] += ((_elemMatMap[elem_se_back] == 0) * elem_se_back) * (1 - flag_lastrow) * (1 - flag_lastcol) * (1 - flag_lastslice)
    end
    # Correction of the numbering of interface nodes and reduction of the vector of fluid elements
    vFluidElems_new = zeros(UInt64, numFluidElems)
    for N = 1:nNodes
        _DOFMap[N] += (_DOFMap[N] > numVelocityNodes) * (-nNodes + numVelocityNodes)
        vFluidElems_new[(N <= numFluidElems) * N + (N > numFluidElems)] += vFluidElems[N] * (N <= numFluidElems)
    end
    # Total number of degrees of freedom
    numDOFs = 4 * numVelocityNodes + numInterfaceNodes
    vFluidElems = nothing
    return numVelocityNodes, numInterfaceNodes, numDOFs, vFluidElems_new
end

# Estimate memory consuption:
function estimateMemory(_model::Model)
    println("   .Estimating memory!")
    # m_matIDs = 16 bits * _model.nElems
    # m_model.DOFMap = 64 bits * _model.nNodes
    # RHS = 64 bits * _model.nDOFs
    # PCGM Solver   / _model.solverType == 0 / M d x q = 4 * 64 bits * _model.nDOFs
    # Direct Solver / _model.solverType == 1 / K = 18 * 64 bits * _model.nElems (rough sparse estimative)
    mem::Float64 = 0.0
    if (_model.solverType == 0)
        mem = (16 * _model.nElems + 64 * _model.nNodes + 5 * 64 * _model.nDOFs) / 8 / 1_000_000
    elseif (_model.solverType == 1)
        mem = (16 * _model.nElems + 64 * _model.nNodes + 2 * 64 * _model.nDOFs + 18 * 64 * _model.nElems) / 8 / 1_000_000
    end
    println("   $(_model.nDOFs) DOFs")
    println("   $mem MB")
    println("---------------------------")
end

# Compute the element matrices for the fluid:
function finiteElementMatrices!(_k::Matrix{Float64}, _g::Matrix{Float64}, _pe::Matrix{Float64}, _f::Vector{Float64}, _SN::Matrix{Float64})
    println("   .Computing FEM matrices!")
    # Initializations
    N = zeros(Float64, 3, 24)
    delta::Float64 = 1.0
    # Compute the matrices
    coordsElem = [ 0. 0. 0.;
                  delta 0. 0.;
                  delta delta 0.;
                  0. delta 0.;
                  0. 0. delta;
                  delta 0. delta;
                  delta delta delta;
                  0. delta delta ];
    rr = [-1.0 / sqrt(3) 1.0 / sqrt(3)]; ss = rr; tt = rr;
    # ww = [1.0 1.0]; # taken out of the computations (multiplying by 1 changes nothing)
    C = [ 2.0 0.0 0.0 0.0 0.0 0.0;
          0.0 2.0 0.0 0.0 0.0 0.0;
          0.0 0.0 2.0 0.0 0.0 0.0;
          0.0 0.0 0.0 1.0 0.0 0.0;
          0.0 0.0 0.0 0.0 1.0 0.0;
          0.0 0.0 0.0 0.0 0.0 1.0 ];
    # h2 = ((dx)^2)+((dy)^2)+((dz)^2);
    h2::Float64 = 3.0
    stab::Float64 = h2 / 18 / 1
    for i = 1:2
        r = rr[1,i]
        for j = 1:2
            s = ss[1,j]
            for l = 1:2
                t = tt[1,l]
                N[1,1]  = 0.125 * (1 - r) * (1 - s) * (1 - t)
                N[1,4]  = 0.125 * (1 + r) * (1 - s) * (1 - t)
                N[1,7]  = 0.125 * (1 + r) * (1 + s) * (1 - t)
                N[1,10] = 0.125 * (1 - r) * (1 + s) * (1 - t)
                N[1,13] = 0.125 * (1 - r) * (1 - s) * (1 + t)
                N[1,16] = 0.125 * (1 + r) * (1 - s) * (1 + t)
                N[1,19] = 0.125 * (1 + r) * (1 + s) * (1 + t)
                N[1,22] = 0.125 * (1 - r) * (1 + s) * (1 + t)
                N[2,2]  = N[1,1]
                N[2,5]  = N[1,4]
                N[2,8]  = N[1,7]
                N[2,11] = N[1,10]
                N[2,14] = N[1,13]
                N[2,17] = N[1,16]
                N[2,20] = N[1,19]
                N[2,23] = N[1,22]
                N[3,3]  = N[1,1]
                N[3,6]  = N[1,4]
                N[3,9]  = N[1,7]
                N[3,12] = N[1,10]
                N[3,15] = N[1,13]
                N[3,18] = N[1,16]
                N[3,21] = N[1,19]
                N[3,24] = N[1,22]
                dN1dr = -0.125 * (1 - s) * (1 - t)
                dN2dr =  0.125 * (1 - s) * (1 - t)
                dN3dr =  0.125 * (1 + s) * (1 - t)
                dN4dr = -0.125 * (1 + s) * (1 - t)
                dN5dr = -0.125 * (1 - s) * (1 + t)
                dN6dr =  0.125 * (1 - s) * (1 + t)
                dN7dr =  0.125 * (1 + s) * (1 + t)
                dN8dr = -0.125 * (1 + s) * (1 + t)
                dN1ds = -0.125 * (1 - r) * (1 - t)
                dN2ds = -0.125 * (1 + r) * (1 - t)
                dN3ds =  0.125 * (1 + r) * (1 - t)
                dN4ds =  0.125 * (1 - r) * (1 - t)
                dN5ds = -0.125 * (1 - r) * (1 + t)
                dN6ds = -0.125 * (1 + r) * (1 + t)
                dN7ds =  0.125 * (1 + r) * (1 + t)
                dN8ds =  0.125 * (1 - r) * (1 + t)
                dN1dt =  -0.125 * (1 - r) * (1 - s)
                dN2dt =  -0.125 * (1 + r) * (1 - s)
                dN3dt =  -0.125 * (1 + r) * (1 + s)
                dN4dt =  -0.125 * (1 - r) * (1 + s)
                dN5dt = 0.125 * (1 - r) * (1 - s)
                dN6dt = 0.125 * (1 + r) * (1 - s)
                dN7dt = 0.125 * (1 + r) * (1 + s)
                dN8dt = 0.125 * (1 - r) * (1 + s)
                DN = [ dN1dr dN2dr dN3dr dN4dr dN5dr dN6dr dN7dr dN8dr ;
                       dN1ds dN2ds dN3ds dN4ds dN5ds dN6ds dN7ds dN8ds ;
                       dN1dt dN2dt dN3dt dN4dt dN5dt dN6dt dN7dt dN8dt ];
                J = DN * coordsElem
                detJ = det(J)
                invJ = inv(J)
                DNxyz = invJ * DN
                B = [ DNxyz[1,1] 0 0 DNxyz[1,2] 0 0 DNxyz[1,3] 0 0 DNxyz[1,4] 0 0 DNxyz[1,5] 0 0 DNxyz[1,6] 0 0 DNxyz[1,7] 0 0 DNxyz[1,8] 0 0;
                      0 DNxyz[2,1] 0 0 DNxyz[2,2] 0 0 DNxyz[2,3] 0 0 DNxyz[2,4] 0 0 DNxyz[2,5] 0 0 DNxyz[2,6] 0 0 DNxyz[2,7] 0 0 DNxyz[2,8] 0;
                      0 0 DNxyz[3,1] 0 0 DNxyz[3,2] 0 0 DNxyz[3,3] 0 0 DNxyz[3,4] 0 0 DNxyz[3,5] 0 0 DNxyz[3,6] 0 0 DNxyz[3,7] 0 0 DNxyz[3,8];
                      DNxyz[2,1] DNxyz[1,1] 0 DNxyz[2,2] DNxyz[1,2] 0 DNxyz[2,3] DNxyz[1,3] 0 DNxyz[2,4] DNxyz[1,4] 0 DNxyz[2,5] DNxyz[1,5] 0 [
                                                                      DNxyz[2,6] ] DNxyz[1,6] 0 DNxyz[2,7] DNxyz[1,7] 0 DNxyz[2,8] DNxyz[1,8] 0;
                      DNxyz[3,1] 0 DNxyz[1,1] DNxyz[3,2] 0 DNxyz[1,2] DNxyz[3,3] 0 DNxyz[1,3] DNxyz[3,4] 0 DNxyz[1,4] DNxyz[3,5] 0 DNxyz[1,5] [
                                                                        DNxyz[3,6] ] 0 DNxyz[1,6] DNxyz[3,7] 0 DNxyz[1,7] DNxyz[3,8] 0 DNxyz[1,8];
                      0 DNxyz[3,1] DNxyz[2,1] 0 DNxyz[3,2] DNxyz[2,2] 0 DNxyz[3,3] DNxyz[2,3] 0 DNxyz[3,4] DNxyz[2,4] 0 DNxyz[3,5] DNxyz[2,5] [
                                                                      0 ] DNxyz[3,6] DNxyz[2,6] 0 DNxyz[3,7] DNxyz[2,7] 0 DNxyz[3,8] DNxyz[2,8];
                       ];
                _k .= _k + B' * C * B * detJ# *ww[1,i]*ww[1,j]*ww[1,l];
                _g .= _g + B' * [1.0;1.0;1.0;0.0;0.0;0.0] * [N[1,1] N[1,4] N[1,7] N[1,10] N[1,13] N[1,16] N[1,19] N[1,22]] * detJ# *ww[1,i]*ww[1,j]*ww[1,l];
                _pe .= _pe + stab * DNxyz' * DNxyz * detJ# *ww[1,i]*ww[1,j]*ww[1,l];
                _f .= _f + [N[1,1];N[1,4];N[1,7];N[1,10];N[1,13];N[1,16];N[1,19];N[1,22]] * detJ# *ww[1,i]*ww[1,j]*ww[1,l];
                _SN .= _SN + N * detJ# *ww[1,i]*ww[1,j]*ww[1,l];
            end
        end
    end
end

# Compute the RHS: Boundary or Domain, _rhsType: Boundary = 0 || Domain = 1,  axis 0 = X || axis 1 = Y || axis 2 = Z
function computeRHS!(_model::Model, _RHS::Vector{Float64}, _axis::Int, _fe::Vector{Float64})
    println("   .Computing RHS!")
    # Initializations
    nx::UInt64 = _model.nx
    ny::UInt64 = _model.ny
    numVelocityNodes::UInt64 = _model.nVelocityNodes
    # Compute each RHS (_axis) based on boundary or domain data (_rhsType)
    n::UInt64 = 0; dof::UInt64 = 0;
    if _model.rhsType == 0  # Domain
        @fastmath @inbounds @simd for e in _model.FluidElems
            n = 2 + (e - 1) % (nx * ny) + div((e - 1) % (nx * ny), ny) + div(e - 1, nx * ny) * (nx + 1) * (ny + 1)
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[1] * (numVelocityNodes >= dof)
            n += ny + 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[2] * (numVelocityNodes >= dof)
            n -= 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[3] * (numVelocityNodes >= dof)
            n -= ny + 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[4] * (numVelocityNodes >= dof)
            n += (nx + 1) * (ny + 1) + 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[5] * (numVelocityNodes >= dof)
            n += ny + 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[6] * (numVelocityNodes >= dof)
            n -= 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[7] * (numVelocityNodes >= dof)
            n -= ny + 1
            dof = _model.DOFMap[n]
            _RHS[(numVelocityNodes >= dof) * (dof * 3 - (3 - _axis)) + 1] += _fe[8] * (numVelocityNodes >= dof)
        end
    end
end

# Jacobi Preconditioner: assembly || M
function jacobiPrecond!(_model::Model, _M::Vector{Float64}, _k::Matrix{Float64}, _p::Matrix{Float64})
    println("   .Jacobi Preconditioner!")
    # Initializations:
    numVelocityNodes::UInt64 = _model.nVelocityNodes
    numVDOFs::UInt64 = 3 * numVelocityNodes
    nx::UInt64 = _model.nx
    ny::UInt64 = _model.ny
    n::UInt64 = 0; dof::UInt64 = 0;
    # Compute the preconditioner: 
    @fastmath @inbounds @simd for e in _model.FluidElems
        n = 2 + (e - 1) % (nx * ny) + div((e - 1) % (nx * ny), ny) + div(e - 1, nx * ny) * (nx + 1) * (ny + 1)
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[1,1]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[i,i] * (numVelocityNodes >= dof)
        end
        n += ny + 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[2,2]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[3 + i,3 + i] * (numVelocityNodes >= dof)
        end
        n -= 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[3,3]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[6 + i,6 + i] * (numVelocityNodes >= dof)
        end
        n -= ny + 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[4,4]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[9 + i,9 + i] * (numVelocityNodes >= dof)
        end
        n += (nx + 1) * (ny + 1) + 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[5,5]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[12 + i,12 + i] * (numVelocityNodes >= dof)
        end
        n += ny + 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[6,6]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[15 + i,15 + i] * (numVelocityNodes >= dof)
        end
        n -= 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[7,7]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[18 + i,18 + i] * (numVelocityNodes >= dof)
        end
        n -= ny + 1
        dof = _model.DOFMap[n]
        _M[numVDOFs + dof] -= _p[8,8]
        for i = 1:3
            _M[(numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1] += _k[21 + i,21 + i] * (numVelocityNodes >= dof)
        end
    end
    _M .= _M .\ 1
end 

# Preconditioned Conjugate Gradient Method:
function pcg_old!(_model::Model, _x::Vector{Float64}, _r::Vector{Float64}, _M::Vector{Float64}, _k::Matrix{Float64}, _g::Matrix{Float64}, _p::Matrix{Float64})
    println("   .PCG Solver!")
    # Initializations
    nx = _model.nx;
    ny = _model.ny;
    numVelocityNodes = _model.nVelocityNodes;
    numVDOFs = 3 * numVelocityNodes;
    d = zeros(Float64, _model.nDOFs);
    q = zeros(Float64, _model.nDOFs);
    pElemDOFNumP = zeros(UInt64, 8);
    pElemDOFNumV = zeros(UInt64, 24);
    isOnlyFluid = zeros(UInt8, 24);
    q_temp = 0;
    # PCG Initialization:
    d .= _r;
    d .*= _M;
    delta_new = dot(_r, d);
    delta_0 = delta_new;
    i_max = _model.pcgIter;
    ii = 0;
    # PCG Iterations:
    while (ii < i_max) && (abs(delta_new) > _model.pcgTol * _model.pcgTol * abs(delta_0))
    # while (ii<=i_max) && (maximum(abs.(_r))>_pcgTol)
        @fastmath @inbounds @simd   for e in _model.FluidElems
            n = 2 + (e - 1) % (nx * ny) + div((e - 1) % (nx * ny), ny) + div(e - 1, nx * ny) * (nx + 1) * (ny + 1);
            dof = _model.DOFMap[n];
            pElemDOFNumP[1] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[i] = (numVelocityNodes >= dof);
            end
            n += ny + 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[2] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[3 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[3 + i] = (numVelocityNodes >= dof);
            end
            n -= 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[3] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[6 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[6 + i] = (numVelocityNodes >= dof);
            end
            n -= ny + 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[4] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[9 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[9 + i] = (numVelocityNodes >= dof);
            end
            n += (nx + 1) * (ny + 1) + 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[5] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[12 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[12 + i] = (numVelocityNodes >= dof);
            end
            n += ny + 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[6] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[15 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[15 + i] = (numVelocityNodes >= dof);
            end
            n -= 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[7] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[18 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[18 + i] = (numVelocityNodes >= dof);
            end
            n -= ny + 1;
            dof = _model.DOFMap[n];
            pElemDOFNumP[8] = numVDOFs + dof;
            for i = 1:3
                pElemDOFNumV[21 + i] = (numVelocityNodes >= dof) * (dof * 3 - (3 - (i - 1))) + 1;
                isOnlyFluid[21 + i] = (numVelocityNodes >= dof);
            end
            for i = 1:24
                if (isOnlyFluid[i] == 1)
                    q_temp = 0;
                    for j = 1:24
                        q_temp += _k[i,j] * d[pElemDOFNumV[j]] * isOnlyFluid[j];
                    end
                    for j = 1:8
                        q_temp += _g[i,j] * d[pElemDOFNumP[j]];
                    end
                    q[pElemDOFNumV[i]] += q_temp;
                end
            end
            for i = 1:8
                q_temp = 0;
                for j = 1:24
                    q_temp += _g[j,i] * d[pElemDOFNumV[j]] * isOnlyFluid[j];
                end
                for j = 1:8
                    q_temp -= _p[i,j] * d[pElemDOFNumP[j]];
                end
                q[pElemDOFNumP[i]] += q_temp;
            end
        end
        alfa = delta_new / dot(d, q);
        d .*= alfa;
        _x .+= d;
        q .*= alfa;
        _r .-= q;
        q .= _r;
        q .*= _M;
        delta_old = delta_new;
        delta_new = dot(_r, q);
        beta = delta_new / delta_old;
        d .*= beta / alfa;
        d .+= q;
        q .*= 0;
        ii += 1;
    end
    println("    $ii steps")
    println("    Residue = ", sqrt(abs(delta_new) / abs(delta_0)))
end

# Preconditioned Conjugate Gradient Method:
function pcg!(_model::Model, _x::Vector{Float64}, _r::Vector{Float64}, _M::Vector{Float64}, _k::Matrix{Float64}, _g::Matrix{Float64}, _p::Matrix{Float64})
    println("   .PCG Solver!")
    # Initializations:    
    d = zeros(Float64, _model.nDOFs)
    q = zeros(Float64, _model.nDOFs)
    pElemDOFNumP = zeros(UInt64, 8)
    pElemDOFNumV = zeros(UInt64, 24)
    pElemDOFNumV_local = zeros(UInt8, 24)
    pElemDOFVarP = zeros(Float64, 8)
    pElemDOFVarV = zeros(Float64, 24)
    isNodeFluid::Bool = false
    dofs = zeros(UInt64, 8)
    count_elemDOFV::UInt64 = 0
    q_temp::Float64 = 0.0
    alfa::Float64 = 0.0
    beta::Float64 = 0.0
    ri::Float64   = 0.0
    qi::Float64   = 0.0
    delta_new::Float64 = 0.0
    delta_old::Float64 = 0.0
    N1::UInt64 = 0; N2::UInt64 = 0; N3::UInt64 = 0; N4::UInt64 = 0;
    N5::UInt64 = 0; N6::UInt64 = 0; N7::UInt64 = 0; N8::UInt64 = 0;
    numVelocityNodes::UInt64 = _model.nVelocityNodes
    numVDOFs::UInt64 = 3 * numVelocityNodes
    nElem_perSlice::UInt64 = _model.nx * _model.ny 
    nNode_perSlice::UInt64 = (_model.nx + 1) * (_model.ny + 1)
    # PCG Initialization:
    @inbounds for i=1:_model.nDOFs; d[i] = _r[i]*_M[i]; end
    delta_new = dot(_r,d)
    delta_0 = delta_new
    if (abs(delta_0)<1e-14); println("    x0 satisfied absolute tolerance criteria: delta < 1e-14"); return; end
    tolerance::Float64 = _model.pcgTol * _model.pcgTol * abs(delta_0)
    iteration_count::UInt64 = _model.pcgIter
    # PCG Iterations:
    for ii = 1:_model.pcgIter
        # (EbE) q = Kd
        @fastmath @inbounds @simd for e in _model.FluidElems
            N1 = 2 + (e - 1) % (nElem_perSlice) + div((e - 1) % (nElem_perSlice), _model.ny) + div(e - 1, nElem_perSlice) * nNode_perSlice;
            N3 = N1 + _model.ny; N2 = N3 + 1; N4 = N1 - 1;
            N5 = N1 + nNode_perSlice; N6 = N2 + nNode_perSlice; N7 = N3 + nNode_perSlice; N8 = N4 + nNode_perSlice;
            dofs[1] = _model.DOFMap[N1]; dofs[2] = _model.DOFMap[N2]; dofs[3] = _model.DOFMap[N3]; dofs[4] = _model.DOFMap[N4];
            dofs[5] = _model.DOFMap[N5]; dofs[6] = _model.DOFMap[N6]; dofs[7] = _model.DOFMap[N7]; dofs[8] = _model.DOFMap[N8];
            count_elemDOFV = 0            
            @inbounds for i = 1:8
                pElemDOFNumP[i] = numVDOFs + dofs[i]
                pElemDOFVarP[i] = d[pElemDOFNumP[i]]
                isNodeFluid = (numVelocityNodes >= dofs[i])
                pElemDOFNumV[count_elemDOFV+1] = 3*dofs[i]-2; pElemDOFNumV[count_elemDOFV+2] = 3*dofs[i]-1; pElemDOFNumV[count_elemDOFV+3] = 3*dofs[i];
                pElemDOFNumV_local[count_elemDOFV+1] = 3*i-2; pElemDOFNumV_local[count_elemDOFV+2] = 3*i-1; pElemDOFNumV_local[count_elemDOFV+3] = 3*i;
                count_elemDOFV += 3*isNodeFluid
                pElemDOFVarV[3*i-2] = isNodeFluid ? d[3*dofs[i]-2] : 0.0
                pElemDOFVarV[3*i-1] = isNodeFluid ? d[3*dofs[i]-1] : 0.0
                pElemDOFVarV[3*i]   = isNodeFluid ?   d[3*dofs[i]] : 0.0
            end
            @inbounds for i = 1:count_elemDOFV
                iii::UInt16 = pElemDOFNumV_local[i]
                q_temp = _k[iii,1] * pElemDOFVarV[1]
                @inbounds for j=2:24; q_temp += _k[iii,j] * pElemDOFVarV[j]; end
                @inbounds for j=1:8;  q_temp += _g[iii,j] * pElemDOFVarP[j]; end
                q[pElemDOFNumV[i]] += q_temp
            end
            @inbounds for i = 1:8
                q_temp = _g[1,i] * pElemDOFVarV[1]
                @inbounds for j=2:24; q_temp += _g[j,i] * pElemDOFVarV[j]; end
                @inbounds for j=1:8;  q_temp -= _p[i,j] * pElemDOFVarP[j]; end
                q[pElemDOFNumP[i]] += q_temp
            end
        end
        alfa = delta_new / dot(d,q)
        delta_old = delta_new
        delta_new = 0.0
        @inbounds for i=1:_model.nDOFs
            _x[i]+=d[i]*alfa
            _r[i]-=q[i]*alfa
            ri   =_r[i]
            qi   =  ri*_M[i]
            q[i]  =qi
            delta_new+=ri*qi       
        end
        if (abs(delta_new) <= tolerance); iteration_count = ii; break; end
        beta = delta_new / delta_old
        @inbounds for i=1:_model.nDOFs
            d[i] *= beta
            d[i] += q[i]
            q[i]  = 0.0
        end
    end
    println("    $iteration_count steps")
    println("    Residue = ", sqrt(abs(delta_new) / abs(delta_0)))
end

# Direct Solver: [K] 64 bits * m_nDOFs * m_nDOFs 
function directMethod!(_model::Model, _x1::Vector{Float64}, _x2::Vector{Float64}, _x3::Vector{Float64}, _RHS1::Vector{Float64}, _RHS2::Vector{Float64}, _RHS3::Vector{Float64}, _K::Matrix{Float64}, _G::Matrix{Float64}, _Pe::Matrix{Float64})
    println("   .Direct Solver!");
    # Initializations:
    numVelocityNodes::UInt64 = _model.nVelocityNodes
    numVDOFs::UInt64 = 3 * numVelocityNodes
    N1::UInt64 = 0;  N2::UInt64 = 0;  N3::UInt64 = 0;  N4::UInt64 = 0;
    N5::UInt64 = 0;  N6::UInt64 = 0;  N7::UInt64 = 0;  N8::UInt64 = 0;
    nN1::UInt64 = 0; nN2::UInt64 = 0; nN3::UInt64 = 0; nN4::UInt64 = 0;
    nN5::UInt64 = 0; nN6::UInt64 = 0; nN7::UInt64 = 0; nN8::UInt64 = 0;
    pElemDOFNum = zeros(UInt64, 32)
    isOnlyFluid = zeros(UInt8, 24)
    A = spzeros(_model.nDOFs, _model.nDOFs)
    # Assembly system matrix:
    @fastmath @inbounds @simd for e in _model.FluidElems
        N1 = e + 1 + div(e - 1, _model.ny);
        N3 = N1 + _model.ny; N2 = N3 + 1; N4 = N1 - 1;
        N5 = N1 + (_model.ny+1)*(_model.nx+1); 
        N7 = N5 + _model.ny; N6 = N7 + 1; N8 = N5 - 1;   
        nN1 = _model.DOFMap[N1]
        nN2 = _model.DOFMap[N2]
        nN3 = _model.DOFMap[N3]
        nN4 = _model.DOFMap[N4]
        nN5 = _model.DOFMap[N5]
        nN6 = _model.DOFMap[N6]
        nN7 = _model.DOFMap[N7]
        nN8 = _model.DOFMap[N8]
        pElemDOFNum[1] = ((numVelocityNodes >= nN1) * (nN1 * 3 - 3)) + 1
        pElemDOFNum[2] = ((numVelocityNodes >= nN1) * (nN1 * 3 - 2)) + 1
        pElemDOFNum[3] = ((numVelocityNodes >= nN1) * (nN1 * 3 - 1)) + 1
        pElemDOFNum[4] = ((numVelocityNodes >= nN2) * (nN2 * 3 - 3)) + 1
        pElemDOFNum[5] = ((numVelocityNodes >= nN2) * (nN2 * 3 - 2)) + 1
        pElemDOFNum[6] = ((numVelocityNodes >= nN2) * (nN2 * 3 - 1)) + 1
        pElemDOFNum[7] = ((numVelocityNodes >= nN3) * (nN3 * 3 - 3)) + 1
        pElemDOFNum[8] = ((numVelocityNodes >= nN3) * (nN3 * 3 - 2)) + 1
        pElemDOFNum[9] = ((numVelocityNodes >= nN3) * (nN3 * 3 - 1)) + 1
        pElemDOFNum[10] = ((numVelocityNodes >= nN4) * (nN4 * 3 - 3)) + 1
        pElemDOFNum[11] = ((numVelocityNodes >= nN4) * (nN4 * 3 - 2)) + 1
        pElemDOFNum[12] = ((numVelocityNodes >= nN4) * (nN4 * 3 - 1)) + 1
        pElemDOFNum[13] = ((numVelocityNodes >= nN5) * (nN5 * 3 - 3)) + 1
        pElemDOFNum[14] = ((numVelocityNodes >= nN5) * (nN5 * 3 - 2)) + 1
        pElemDOFNum[15] = ((numVelocityNodes >= nN5) * (nN5 * 3 - 1)) + 1
        pElemDOFNum[16] = ((numVelocityNodes >= nN6) * (nN6 * 3 - 3)) + 1
        pElemDOFNum[17] = ((numVelocityNodes >= nN6) * (nN6 * 3 - 2)) + 1
        pElemDOFNum[18] = ((numVelocityNodes >= nN6) * (nN6 * 3 - 1)) + 1
        pElemDOFNum[19] = ((numVelocityNodes >= nN7) * (nN7 * 3 - 3)) + 1
        pElemDOFNum[20] = ((numVelocityNodes >= nN7) * (nN7 * 3 - 2)) + 1
        pElemDOFNum[21] = ((numVelocityNodes >= nN7) * (nN7 * 3 - 1)) + 1
        pElemDOFNum[22] = ((numVelocityNodes >= nN8) * (nN8 * 3 - 3)) + 1
        pElemDOFNum[23] = ((numVelocityNodes >= nN8) * (nN8 * 3 - 2)) + 1
        pElemDOFNum[24] = ((numVelocityNodes >= nN8) * (nN8 * 3 - 1)) + 1
        pElemDOFNum[25] = numVDOFs + nN1
        pElemDOFNum[26] = numVDOFs + nN2
        pElemDOFNum[27] = numVDOFs + nN3
        pElemDOFNum[28] = numVDOFs + nN4
        pElemDOFNum[29] = numVDOFs + nN5
        pElemDOFNum[30] = numVDOFs + nN6
        pElemDOFNum[31] = numVDOFs + nN7
        pElemDOFNum[32] = numVDOFs + nN8
        isOnlyFluid[1] = (numVelocityNodes >= nN1)
        isOnlyFluid[2] = (numVelocityNodes >= nN1)
        isOnlyFluid[3] = (numVelocityNodes >= nN1)
        isOnlyFluid[4] = (numVelocityNodes >= nN2)
        isOnlyFluid[5] = (numVelocityNodes >= nN2)
        isOnlyFluid[6] = (numVelocityNodes >= nN2)
        isOnlyFluid[7] = (numVelocityNodes >= nN3)
        isOnlyFluid[8] = (numVelocityNodes >= nN3)
        isOnlyFluid[9] = (numVelocityNodes >= nN3)
        isOnlyFluid[10] = (numVelocityNodes >= nN4)
        isOnlyFluid[11] = (numVelocityNodes >= nN4)
        isOnlyFluid[12] = (numVelocityNodes >= nN4)
        isOnlyFluid[13] = (numVelocityNodes >= nN5)
        isOnlyFluid[14] = (numVelocityNodes >= nN5)
        isOnlyFluid[15] = (numVelocityNodes >= nN5)
        isOnlyFluid[16] = (numVelocityNodes >= nN6)
        isOnlyFluid[17] = (numVelocityNodes >= nN6)
        isOnlyFluid[18] = (numVelocityNodes >= nN6)
        isOnlyFluid[19] = (numVelocityNodes >= nN7)
        isOnlyFluid[20] = (numVelocityNodes >= nN7)
        isOnlyFluid[21] = (numVelocityNodes >= nN7)
        isOnlyFluid[22] = (numVelocityNodes >= nN8)
        isOnlyFluid[23] = (numVelocityNodes >= nN8)
        isOnlyFluid[24] = (numVelocityNodes >= nN8)
        for i = 1:24
            for j = 1:24
                A[pElemDOFNum[i],pElemDOFNum[j]] += _K[i,j] * isOnlyFluid[i] * isOnlyFluid[j]
            end
        end
        for i = 1:8
            for j = 1:8
                A[pElemDOFNum[i + 24],pElemDOFNum[j + 24]] += -_Pe[i,j]
            end
        end
        for i = 1:24
            for j = 1:8
                A[pElemDOFNum[i],pElemDOFNum[j + 24]] += _G[i,j] * isOnlyFluid[i]
            end
        end
        for i = 1:8
            for j = 1:24
                A[pElemDOFNum[i + 24],pElemDOFNum[j]] += _G[j,i] * isOnlyFluid[j]
            end
        end
    end
    # Solve for three rhs:
    _x1 .= A \ _RHS1
    _x2 .= A \ _RHS2
    _x3 .= A \ _RHS3
end

# Compute Velocity-FEM Effective property
function femEffective(_model::Model, _T::Vector{Float64}, _axis::Int, _SN::Matrix{Float64})
    println("   .Updating Effective Property!")
    # Initializations
    nx::UInt64 = _model.nx
    ny::UInt64 = _model.ny
    nz::UInt64 = _model.nz
    numVelocityNodes::UInt64 = _model.nVelocityNodes
    delta::Float64 = 1.0
    Qx::Float64 = 0; Qy::Float64 = 0; Qz::Float64 = 0;
    n::UInt64 = 0; dof::UInt64 = 0;
    # loop over the fluid elements
    @fastmath @inbounds @simd for e in _model.FluidElems
        n = 2 + (e - 1) % (nx * ny) + div((e - 1) % (nx * ny), ny) + div(e - 1, nx * ny) * (nx + 1) * (ny + 1)
        dof = _model.DOFMap[n]
        Qx += _SN[1,1] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,2] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,3] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n += ny + 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,4] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,5] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,6] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n -= 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,7] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,8] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,9] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n -= ny + 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,10] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,11] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,12] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n += (nx + 1) * (ny + 1) + 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,13] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,14] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,15] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n += ny + 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,16] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,17] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,18] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n -= 1
        dof = _model.DOFMap[n]
        Qx += _SN[1,19] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,20] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,21] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
        n -= ny + 1
        dof = _model.DOFMap[n];
        Qx += _SN[1,22] * _T[(numVelocityNodes >= dof) * (dof * 3 - 3) + 1] * (numVelocityNodes >= dof)
        Qy += _SN[2,23] * _T[(numVelocityNodes >= dof) * (dof * 3 - 2) + 1] * (numVelocityNodes >= dof)
        Qz += _SN[3,24] * _T[(numVelocityNodes >= dof) * (dof * 3 - 1) + 1] * (numVelocityNodes >= dof)
    end
    V::Float64 =  nx * ny * nz
    C = zeros(Float64,3,3)
    if _axis == 0
        C = [ Qx / V 0.0 0.0 ; Qy / V 0.0 0.0 ; Qz / V 0.0 0.0 ]
    elseif _axis == 1
        C = [ 0.0 Qx / V 0.0 ; 0.0 Qy / V 0.0 ; 0.0 Qz / V 0.0 ]
    elseif _axis == 2
        C = [ 0.0 0.0 Qx / V ; 0.0 0.0 Qy / V ; 0.0 0.0 Qz / V ]
    end
    return C * _model.voxelSize * _model.voxelSize / (_model.refinement  * _model.refinement )
end

# -----------------
function homogenize(_arg)
    println("---------------------------")
    # Build the Model data struct:
    m_model = buildModel(_arg * ".json", _arg * ".raw")
    # Estimate Memory Consumption:
    estimateMemory(m_model)
    # SOLVE:
    println(".Solving")
    # Compute FEM matrices for fluids:
    m_K = zeros(Float64, 24, 24);       m_G = zeros(Float64, 24, 8);       m_Pe = zeros(Float64, 8, 8);
    m_F = zeros(Float64, 8);            m_SN = zeros(Float64, 3, 24);      m_C = zeros(Float64, 3, 3);
    finiteElementMatrices!(m_K, m_G, m_Pe, m_F, m_SN)
    if (m_model.solverType == 0) # Preconditioned Conjugate Gradient Method
        # Initialize the effective tensor, the right hand side, the inicial guess and the preconditioner:
        m_M = zeros(Float64, m_model.nDOFs) 
        m_X = zeros(Float64, m_model.nDOFs)    
        m_RHS = zeros(Float64, m_model.nDOFs)
        # Compute the Jacobi preconditioner:
        jacobiPrecond!(m_model, m_M, m_K, m_Pe)
        #m_M = ones(Float64, m_model.nDOFs);
        for axis = 0:2
            println("\n  Case ", axis + 1)
            # Compute the RHS: Boundary or Domain, rhsType: Boundary = 1 || Domain = 0, axis 0 = X || axis 1 = Y 
            computeRHS!(m_model, m_RHS, axis, m_F)  
            # Solver (to ensure optimal RAM usage we call GC before and after the PCGM):    
            GC.gc()
            #pcg_old!(m_model, m_X, m_RHS, m_M, m_K, m_G, m_Pe);
            pcg!(m_model, m_X, m_RHS, m_M, m_K, m_G, m_Pe)
            GC.gc()
            # Compute Effective Property:
            m_C .+= femEffective(m_model, m_X, axis, m_SN)
            m_RHS .*= 0
            m_X .*= 0
        end
        m_M = nothing; m_X = nothing; m_RHS = nothing;
    elseif (m_model.solverType == 1) # Direct Method
        # Compute the RHS: Boundary or Domain, m_rhsType: Boundary = 0 || Domain = 1, axis 0 = X || axis 1 = Y
        m_RHS1 = zeros(Float64, m_model.nDOFs); m_RHS2 = zeros(Float64, m_model.nDOFs); m_RHS3 = zeros(Float64, m_model.nDOFs);
        computeRHS!(m_model, m_RHS1, 0, m_F)
        computeRHS!(m_model, m_RHS2, 1, m_F)
        computeRHS!(m_model, m_RHS3, 2, m_F)
        # Solver
        m_X1 = zeros(Float64, m_model.nDOFs); m_X2 = zeros(Float64, m_model.nDOFs); m_X3 = zeros(Float64, m_model.nDOFs);
        directMethod!(m_model, m_X1, m_X2, m_X3, m_RHS1, m_RHS2, m_RHS3, m_K, m_G, m_Pe)
        m_RHS1 = nothing; m_RHS2 = nothing; m_RHS3 = nothing;
        # Compute Effective Property:
        m_C .+= femEffective(m_model, m_X1, 0, m_SN)
        m_C .+= femEffective(m_model, m_X2, 1, m_SN)
        m_C .+= femEffective(m_model, m_X3, 2, m_SN)
        m_X1 = nothing; m_X2 = nothing; m_X3 = nothing;
    end
    println("---------------------------")
    println("Effective Properties:\n")
    for i = 1:3
        println("C[$i,:] = ", m_C[i,:])
    end
    println("\n--------------------------------------")
    return m_C
end